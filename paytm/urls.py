from django.urls import path
from . import views


urlpatterns = [

    #url(r'^$', 'paytm.views.home', name='home'),
    path('payment/', views.payment , name='payment'),
    path('response/', views.response , name='response'),
]
